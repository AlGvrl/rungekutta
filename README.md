### About
An implementation of a [Runge-Kutta method](https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods) for integration of arbitrary function and its comparison with Euler method.

### Installation

1. `git clone https://AlGvrl@bitbucket.org/AlGvrl/rungekutta.git`
2. `cd` into project directory.
3. `npm install`
4. `npm start`
5. Open http://localhost:8080/ in your browser.