'use strict';

function integrate (fn, from, to, y0, h) {
    var result = integrateAllSteps(fn, from, to, y0, h);
    return result[result.length - 1].y;
}

function integrateAllSteps (fn, from, to, y0, h) {
    var step = 1,
        result = [{x: from, y: y0}];

    while (step * h + from <= to) {
        var prev = result[result.length - 1],
            k1 = fn(prev.x, prev.y),
            k2 = fn(prev.x + h / 2, prev.y + h / 2 * k1),
            k3 = fn(prev.x + h / 2, prev.y + h / 2 * k2),
            k4 = fn(prev.x + h, prev.y + h * k3),
            newY = prev.y + h / 6 * (k1 + 2 * k2 + 2 * k3 + k4);
        result.push({x: step * h + from, y: newY});
        step++;
    }

    return result;
}

exports.integrate = integrate;
exports.integrateAllSteps = integrateAllSteps;