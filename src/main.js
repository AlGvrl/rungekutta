var d3 = require('d3'),
    nvd3 = require('nvd3'),
    euler = require('./euler.js'),
    rungeKutta = require('./rungeKutta.js'),
    math = require('mathjs'),
    query = parseQueryString(),
    fnText = decodeURIComponent(query.derivative || '2 * x'),
    fn = math.compile(fnText),
    fnWrapper = (function(fn, x, y) {return fn.eval({x: x, y: y});}).bind(undefined, fn),
    from = Number(decodeURIComponent(query.from)) || 0,
    to = Number(decodeURIComponent(query.to)) || 4,
    y0 = Number(decodeURIComponent(query.y0)) || 0,
    h = Number(decodeURIComponent(query.h)) || 0.4,
    roundTo = Number(decodeURIComponent(query.roundTo)) || 2;
    eulerInput = euler.integrateAllSteps(fnWrapper, from, to, y0, h)
      .map(function(val) {
        return {x: +(val.x.toFixed(roundTo)), y: +(val.y.toFixed(roundTo))};
      }),
    rungeKuttaInput = rungeKutta.integrateAllSteps(fnWrapper, from, to, y0, h)
      .map(function(val) {
        return {x: +(val.x.toFixed(roundTo)), y: +(val.y.toFixed(roundTo))};
      });

populateInputs();
document.getElementById('goBtn').addEventListener('click', saveAndRefresh);

nv.addGraph(function() {
    var chart = nv.models.lineChart()
        .margin({left: 100})
        .useInteractiveGuideline(true)
        .showLegend(true)
        .showYAxis(true)
        .showXAxis(true)
    ;

  chart.xAxis
      .axisLabel('X')
      .tickFormat(d3.format(',r'));

  chart.yAxis
      .axisLabel('Y')
      .tickFormat(d3.format(',r'));

  var myData = [
    {
      values: eulerInput,
      key: 'Euler method',
      color: '#995555'
    },
    {
      values: rungeKuttaInput,
      key: 'Runge-Kutta method',
      color: '#555599'
    }
  ];

  d3.select('#chart1').append('svg')    //Select the <svg> element you want to render the chart in.   
      .datum(myData)         //Populate the <svg> element with chart data...
      .call(chart);          //Finally, render the chart!

  //Update the chart when window resizes.
  nv.utils.windowResize(function() { chart.update() });
  return chart;
});

function parseQueryString() {
  var query = location.search.substr(1);
  var result = {};
  if (query) {
    query.split("&").forEach(function(part) {
      var item = part.split("=");
      result[item[0]] = decodeURIComponent(item[1]);
    });
  }
  return result;
}

function populateInputs() {
  document.getElementById('derivative').value = fnText;
  document.getElementById('from').value = from;
  document.getElementById('to').value = to;
  document.getElementById('y0').value = y0;
  document.getElementById('h').value = h;
  document.getElementById('round').value = roundTo;
}

function saveAndRefresh() {
  var newUrl = location.protocol + '//' + location.host + location.pathname + '?';
  newUrl += 'derivative=' + document.getElementById('derivative').value;
  newUrl += '&from=' + document.getElementById('from').value;
  newUrl += '&to=' + document.getElementById('to').value;
  newUrl += '&y0=' + document.getElementById('y0').value;
  newUrl += '&h=' + document.getElementById('h').value;
  newUrl += '&round=' + document.getElementById('round').value;
  location = newUrl;
}