'use strict';

function integrate (fn, from, to, y0, h) {
    var result = integrateAllSteps(fn, from, to, y0, h);
    return result[result.length - 1].y;
}

function integrateAllSteps (fn, from, to, y0, h) {
    var step = 1,
        result = [{x: from, y: y0}];

    while (step * h + from <= to) {
        var prev = result[result.length - 1];
        result.push({x: step * h + from, y: prev.y + h * fn(prev.x, prev.y)});
        step++;
    }

    return result;
}

exports.integrate = integrate;
exports.integrateAllSteps = integrateAllSteps;